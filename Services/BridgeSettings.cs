﻿/* SPDX-License-Identifier: CC-BY-NC-SA-4.0 */
using System.Collections.Generic;
using System.Net;

namespace HapticsWebBridge.Services
{
    public class BridgeSettings
    {
        public const uint MIN_COMMAND_RATE = 50;

        public string[] AllowedIPs { get; set; }
        public string SecretKey { get; set; }
        public DeviceSettings[] DeviceSettings { get; set; }
        public uint WebSocketPort { get; set; }

        public static BridgeSettings Instance;

        Dictionary<string, DeviceSettings> _devicesettings;
        public BridgeSettings()
        {
            Instance = this;
        }

        public DeviceSettings GetDevice(string name)
        {
            Build();

            if (_devicesettings.ContainsKey(name))
                return _devicesettings[name];

            return null;
        }

        void Build()
        {
            if (_devicesettings != null)
                return;

            _devicesettings = new Dictionary<string, DeviceSettings>();

            foreach (DeviceSettings entry in DeviceSettings)
                _devicesettings.Add(entry.Name, entry);
        }
    }

    public class DeviceSettings
    {
        public uint? CommandRate { get; set; }
        public string Name { get; set; }
        public uint? PowerFactor { get; set; }
        public string VisibleName { get; set; }
    }
}
